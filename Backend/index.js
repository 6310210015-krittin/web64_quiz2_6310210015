const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user: 'runner_admin',
    password: 'runner_admin',
    database: 'TimeIn_TimeOut'
});

connection.connect()

const express = require('express')
const res = require('express/lib/response')
const app = express()
const port = 4000


/********** Midlewere **********/
function authenticateToken(req, res, next) {
    const authenheader = req.header['authorization']
    const token = authenheader && authenheader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err){ 
            return res.sendStatus(403) 
        }else {
            req.user = user
            next()
        }
    })
    }

    /**********  Query 1 list all register user ***********/
    app.get("/list_registeruser", (req, res) => {

        let query = `
        SELECT register_user.User_id, register_user.User_name, register_user.User_surname, 
               Working_Times.Working_start, Working_Times.Working_end
        FROM register_user, Working_Times
        WHERE (register_user.User_id = Working_Times.User_id)`;

        connection.query( query, (err, rows) => {
            if (err){
                res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
            }else {
                res.json(rows)
            }
        });
    })


/*********Login **********/
app.post("/login", (req, res) => {
    let User_name = req.query.User_name
    let User_password = req.query.User_password
    let query = `Select * from Runner Where Username='${User_name}'`
    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(User_password, db_password, (err, result) =>{
                if (result) {
                    let payload = {
                        "user_name" : rows[0].User_name,
                        "user_id" : rows[0].User_id,
                        "IsAdmin" : rows[0].Isadmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                    res.send(token)
                }else {res.send("Invalid username / password")}
            })
        }
    })
})

/********* list workingtimes **********/
app.get("/list_workingtimes",(req,res) => {
    let query = `SELECT * from TimeIn_TimeOut`
    connection.query(query,(err,rows) => {
        if(err){
            res.json({
                "STATUS" : "400",
                "MESSAGE" : "Error querying from timein_timeout db"
            })
        }else{
            res.json(rows)
        }
    })
})

/********* register **********/
app.post("/register_userdata", (req,res) => {

    let User_id = req.query.User_id
    let User_name = req.query.User_name
    let User_surname = req.query.User_surname
    let User_password = req.query.User_password

    bcrypt.hash(runner_password, SALT_ROUNDS, (err, hash) => {
    let query =`Insert into User empoyees
                User_id='${User_id}' 
                User_name='${User_name}'
                User_surname='${User_surname}'
                User_password='${User_password}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                "status" : "400",
                "message" : "Error Inserting data to db"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Inserting User empoyees succesful"
            })
        }
    });
  })
});

/********* Insert **********/
app.post("/add_workingtimes", authenticateToken, (req,res) => {

    let User_id = req.query.User_id
    let Working_start = req.query.Working_start
    let Working_end = req.query.Working_end

    let query =`Insert to Working Times 
                User_id='${User_id}
                Working_start='${Working_start}''
                Working_end='${Working_end}'`

    console.log(query)
    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                "status" : "400",
                "message" : "Error Selecting Times from db"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Selecting Times succesful"
            })
        }
    });

})

/********* Update **********/
app.post("/update_userdata",(req,res) => {

    let User_id = req.query.User_id
    let User_name = req.query.User_name
    let User_surname = req.query.User_surname

    let query =`Update User empoyees SET
                User_id=${User_id}, 
                User_name=${User_name}
                User_surname=${User_surname}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                "status" : "400",
                "message" : "Error Updating record"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating User Data succesful"
            })
        }
    });
})

/********* Delete **********/
app.post("/delete_userdata", (req, res) => {

    let User_id = req.query.User_id
    
    let query =`Delete from Userdata
                User_id=${User_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deletting User ID succesful"
            })
        }
    });
})


app.listen(port, () =>{
    console.log(`Now starting TimeIn_TimeOut System Backend ${port} `)
})